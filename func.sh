#!/bin/bash

menu() {
echo 'como deseja alterar a variável PS1?: '
echo '1) Alterar cor'
echo '2) Exibir só o nome do usuário'
echo '3) 2 linhas de prompt'
read -p 'selecione uma opção (1-3): ' opt
case $opt in
	1) cor;;
	2) PS1="\u@\$";;
	3) PS1="\u@\h \W \n \$ ";;
	*) echo 'opção inválida...';;
	
esac

}

cor() {
echo 'Opções de cores: '	
echo '1) vermelho'
echo '2) verde'
echo '3) azul'
echo '4) amarelo'
read -p ' Qual que vc deseja: ' color
case $color in 
	1) PS1="\[\033[0;31m\]\u@\h \W \\$";;
	2) PS1="\[\033[0;32m\]\u@\h \W \\$";;
	3) PS1="\[\033[0;34m\]\u@\h \W \\$";;
	4) PS1="\[\033[0;33m\]\u@\h \W \\$";;
	*) echo 'não tem essa cor...'
esac
}
aplica() {
export PS1
}


reset_ps1() {
    PS1="\u@\h \W \$ "
    export PS1
}

